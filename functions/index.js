const functions = require('firebase-functions');

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


'use strict';

const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

exports.incrementMessageCount = functions.database.ref('/node_chat_messages/{chatId}').onWrite(
    event => {
        var chatId = event.params.chatId
        var memberListRef = admin.database().ref('/node_chat/' + chatId + '/membersList')
        var updates = {}
        return memberListRef.once('value').then(snapshot=> {
            snapshot.forEach(function(child){
                var previousValue = child.val().message_count;
                if(previousValue == undefined)
                    previousValue = event.data.numChildren()
                updates[child.key + '/message_count'] = previousValue+1
            })
        }).then(snapshot => memberListRef.update(updates))
    }
)
